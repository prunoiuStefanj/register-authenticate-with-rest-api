package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloWorldBootSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelloWorldBootSpringApplication.class, args);
    }
}